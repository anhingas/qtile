
# If using transparency, make sure you add (background="#00000000") to 'Screen' line(s).
# Then, you can use RGBA color codes to add transparency to the colors below.
# For ex: colors = [["#282c34ee", "#282c34dd"], ...

DoomOne = [
    ["#282c34", "#282c34"], # Color 01 bg
    ["#bbc2cf", "#bbc2cf"], # Color 02 fg
    ["#1c1f24", "#1c1f24"], # Color 03 bg2 
    ["#ff6c6b", "#ff6c6b"], # Color 04 Red 
    ["#98be65", "#98be65"], # Color 05 Green 
    ["#da8548", "#da8548"], # Color 06 Orange 
    ["#51afef", "#51afef"], # Color 07 Blue 
    ["#c678dd", "#c678dd"], # Color 08 Purple
    ["#46d9ff", "#46d9ff"]  # Color 09 Light Blue
    ]

Dracula  = [
    ["#282a36", "#282a36"], # bg
    ["#f8f8f2", "#f8f8f2"], # fg
    ["#000000", "#000000"], # color01
    ["#ff5555", "#ff5555"], # color02
    ["#50fa7b", "#50fa7b"], # color03
    ["#f1fa8c", "#f1fa8c"], # color04
    ["#bd93f9", "#bd93f9"], # color05
    ["#ff79c6", "#ff79c6"], # color06
    ["#9aedfe", "#9aedfe"]  # color15
    ]

CatppuccinLatte  = [
    ["#dc8a78", "#dc8a78"], # Color 01 Rosewater   
    ["#dd7878", "#dd7878"], # Color 02 Flamingo    
    ["#ea76cb", "#ea76cb"], # Color 03 Pink        
    ["#8839ef", "#8839ef"], # Color 04 Mauve       
    ["#d20f39", "#d20f39"], # Color 05 Red         
    ["#e64553", "#e64553"], # Color 06 Maroon      
    ["#fe640b", "#fe640b"], # Color 07 Peach       
    ["#df8e1d", "#df8e1d"], # Color 08 Yellow      
    ["#40a02b", "#40a02b"], # Color 09 Green       
    ["#179299", "#179299"], # Color 10 Teal        
    ["#04a5e5", "#04a5e5"], # Color 11 Sky         
    ["#209fb5", "#209fb5"], # Color 12 Sapphire    
    ["#1e66f5", "#1e66f5"], # Color 13 Blue        
    ["#7287fd", "#7287fd"], # Color 14 Lavender    
    ["#4c4f69", "#4c4f69"], # Color 15 Text        
    ["#5c5f77", "#5c5f77"], # Color 16 Subtext1    
    ["#6c6f85", "#6c6f85"], # Color 17 Subtext0    
    ["#7c7f93", "#7c7f93"], # Color 18 Overlay2    
    ["#8c8fa1", "#8c8fa1"], # Color 19 Overlay1    
    ["#9ca0b0", "#9ca0b0"], # Color 20 Overlay0    
    ["#acb0be", "#acb0be"], # Color 21 Surface2    
    ["#bcc0cc", "#bcc0cc"], # Color 22 Surface1    
    ["#ccd0da", "#ccd0da"], # Color 23 Surface0    
    ["#eff1f5", "#eff1f5"], # Color 24 Base        
    ["#e6e9ef", "#e6e9ef"], # Color 25 Mantle      
    ["#dce0e8", "#dce0e8"]  # Color 26 Crust       
    # Source - https://catppuccin.ryanccn.dev/palette
    ]

CatppuccinFrappe  = [
    ["#51576d", "#51576d"], # Color 22 Surface1    0
    ["#e78284", "#e78284"], # Color 05 Red         1
    ["#a6d189", "#a6d189"], # Color 09 Green       2
    ["#e5c890", "#e5c890"], # Color 08 Yellow      3
    ["#8caaee", "#8caaee"], # Color 13 Blue        4
    ["#f4b8e4", "#f4b8e4"], # Color 03 Pink        5
    ["#b5bfe2", "#b5bfe2"], # Color 16 Subtext1    6
    ["#626880", "#626880"], # Color 21 Surface2    7
    ["#f2d5cf", "#f2d5cf"], # Color 01 Rosewater   8
    ["#eebebe", "#eebebe"], # Color 02 Flamingo    9
    ["#ca9ee6", "#ca9ee6"], # Color 04 Mauve       
    ["#ea999c", "#ea999c"], # Color 06 Maroon      
    ["#ef9f76", "#ef9f76"], # Color 07 Peach       
    ["#81c8be", "#81c8be"], # Color 10 Teal        
    ["#99d1db", "#99d1db"], # Color 11 Sky         
    ["#85c1dc", "#85c1dc"], # Color 12 Sapphire    
    ["#babbf1", "#babbf1"], # Color 14 Lavender    
    ["#c6d0f5", "#c6d0f5"], # Color 15 Text        
    ["#a5adce", "#a5adce"], # Color 17 Subtext0    
    ["#949cbb", "#949cbb"], # Color 18 Overlay2    
    ["#838ba7", "#838ba7"], # Color 19 Overlay1    
    ["#737994", "#737994"], # Color 20 Overlay0    
    ["#414559", "#414559"], # Color 23 Surface0    
    ["#303446", "#303446"], # Color 24 Base        
    ["#292c3c", "#292c3c"], # Color 25 Mantle      
    ["#232634", "#232634"]  # Color 26 Crust       
    # Source - https://catppuccin.ryanccn.dev/palette
    ]

CatppuccinMacchiato  = [
    ["#f4dbd6", "#f4dbd6"], # Color 01 Rosewater   
    ["#f0c6c6", "#f0c6c6"], # Color 02 Flamingo    
    ["#f5bde6", "#f5bde6"], # Color 03 Pink        
    ["#c6a0f6", "#c6a0f6"], # Color 04 Mauve       
    ["#ed8796", "#ed8796"], # Color 05 Red         
    ["#ee99a0", "#ee99a0"], # Color 06 Maroon      
    ["#f5a97f", "#f5a97f"], # Color 07 Peach       
    ["#eed49f", "#eed49f"], # Color 08 Yellow      
    ["#a6da95", "#a6da95"], # Color 09 Green       
    ["#8bd5ca", "#8bd5ca"], # Color 10 Teal        
    ["#91d7e3", "#91d7e3"], # Color 11 Sky         
    ["#7dc4e4", "#7dc4e4"], # Color 12 Sapphire    
    ["#8aadf4", "#8aadf4"], # Color 13 Blue        
    ["#b7bdf8", "#b7bdf8"], # Color 14 Lavender    
    ["#cad3f5", "#cad3f5"], # Color 15 Text        
    ["#b8c0e0", "#b8c0e0"], # Color 16 Subtext1    
    ["#a5adcb", "#a5adcb"], # Color 17 Subtext0    
    ["#939ab7", "#939ab7"], # Color 18 Overlay2    
    ["#8087a2", "#8087a2"], # Color 19 Overlay1    
    ["#6e738d", "#6e738d"], # Color 20 Overlay0    
    ["#5b6078", "#5b6078"], # Color 21 Surface2    
    ["#494d64", "#494d64"], # Color 22 Surface1    
    ["#363a4f", "#363a4f"], # Color 23 Surface0    
    ["#24273a", "#24273a"], # Color 24 Base        
    ["#1e2030", "#1e2030"], # Color 25 Mantle      
    ["#181926", "#181926"]  # Color 26 Crust       
    # Source - https://catppuccin.ryanccn.dev/palette
    ]

CatppuccinMocha  = [
    ["#f5e0dc", "#f5e0dc"], # Color 01 Rosewater   
    ["#f2cdcd", "#f2cdcd"], # Color 02 Flamingo    
    ["#f5c2e7", "#f5c2e7"], # Color 03 Pink        
    ["#cba6f7", "#cba6f7"], # Color 04 Mauve       
    ["#f38ba8", "#f38ba8"], # Color 05 Red         
    ["#eba0ac", "#eba0ac"], # Color 06 Maroon      
    ["#fab387", "#fab387"], # Color 07 Peach       
    ["#f9e2af", "#f9e2af"], # Color 08 Yellow      
    ["#a6e3a1", "#a6e3a1"], # Color 09 Green       
    ["#94e2d5", "#94e2d5"], # Color 10 Teal        
    ["#89dceb", "#89dceb"], # Color 11 Sky         
    ["#74c7ec", "#74c7ec"], # Color 12 Sapphire    
    ["#89b4fa", "#89b4fa"], # Color 13 Blue        
    ["#b4befe", "#b4befe"], # Color 14 Lavender    
    ["#cdd6f4", "#cdd6f4"], # Color 15 Text        
    ["#bac2de", "#bac2de"], # Color 16 Subtext1    
    ["#a6adc8", "#a6adc8"], # Color 17 Subtext0    
    ["#9399b2", "#9399b2"], # Color 18 Overlay2    
    ["#7f849c", "#7f849c"], # Color 19 Overlay1    
    ["#6c7086", "#6c7086"], # Color 20 Overlay0    
    ["#585b70", "#585b70"], # Color 21 Surface2    
    ["#45475a", "#45475a"], # Color 22 Surface1    
    ["#313244", "#313244"], # Color 23 Surface0    
    ["#1e1e2e", "#1e1e2e"], # Color 24 Base        
    ["#181825", "#181825"], # Color 25 Mantle      
    ["#11111b", "#11111b"]  # Color 26 Crust       
    # Source - https://catppuccin.ryanccn.dev/palette
    ]

KanawagaDragon  = [
    ["#181616", "#181616"], # Color 01 background-color
    ["#C5C9C5", "#C5C9C5"], # Color 02 foreground-color
    ["#181616", "#181616"], # Color 03 badge-color
    ["#72A7BC", "#72A7BC"], # Color 04 bold-color
    ["#C8C093"  "#C8C093"], # Color 05 cursor-color
    ["#2D4F67", "#2D4F67"], # Color 06 highlight-background-color
    ["#C8C093", "#C8C093"], # Color 07 highlight-foreground-color
    ["#0D0C0C", "#0D0C0C"], # Color 08
    ["#C4746E", "#C4746E"], # Color 09
    ["#8A9A7B", "#8A9A7B"], # Color 10
    ["#C4B28A", "#C4B28A"], # Color 11
    ["#8BA4B0", "#8BA4B0"], # Color 12
    ["#A292A3", "#A292A3"], # Color 13
    ["#8EA4A2", "#8EA4A2"], # Color 14
    ["#C8C093", "#C8C093"], # Color 15
    ["#A6A69C", "#A6A69C"], # Color 16
    ["#E46876", "#E46876"], # Color 17
    ["#87A987", "#87A987"], # Color 18
    ["#E6C384", "#E6C384"], # Color 19
    ["#7FB4CA", "#7FB4CA"], # Color 20
    ["#938AA9", "#938AA9"], # Color 21
    ["#7AA89F", "#7AA89F"], # Color 22
    ["#C5C9C5"  "#C5C9C5"]  # Color 23
    # Source - https://github.com/rebelot/kanagawa.nvim/blob/master/extras/Kanagawa%20Dragon.json
    ]

Kanagawa  = [
    ["#DCD7BA", "#ffffff"], # fuji White		
    ["#C8C093", "#ffffff"], # old- white		
    ["#16161D", "#000000"], # sumiInk-0		
    ["#181820", "#000000"], # sumiInk-1b		
    ["#1F1F28", "#080808"], # sumiInk-1		
    ["#2A2A37", "#121212"], # sumiInk-2		
    ["#363646", "#303030"], # sumiInk-3		
    ["#54546D", "#303030"], # sumiInk-4		
    ["#223249", "#4e4e4e"], # wave Blue1		
    ["#2D4F67", "#585858"], # wave Blue2		
    ["#6A9589", "#6a9589"], # wave Aqua1		
    ["#7AA89F", "#717C7C"], # wave Aqua2		
    ["#2B3328", "#585858"], # winter Green		
    ["#49443C", "#585858"], # winter Yellow		
    ["#43242B", "#585858"], # winter Red		
    ["#252535", "#585858"], # winter Blue		
    ["#76946A", "#585858"], # autumn Green		
    ["#C34043", "#585858"], # autumn Red		
    ["#DCA561", "#585858"], # autumn Yellow		
    ["#E82424", "#585858"], # samurai Red		
    ["#FF9E3B", "#585858"], # ronin Yellow		
    ["#658594", "#658594"], # dragon Blue		
    ["#727169", "#717C7C"], # fuji Gray         
    ["#938AA9", "#717C7C"], # spring Violet1	
    ["#957FB8", "#717C7C"], # oni Violet		
    ["#7E9CD8", "#717C7C"], # crystal Blue		
    ["#9CABCA", "#717C7C"], # spring Violet2	
    ["#7FB4CA", "#717C7C"], # spring Blue		
    ["#A3D4D5", "#717C7C"], # light Blue		
    ["#98BB6C", "#717C7C"], # spring Green		
    ["#938056", "#717C7C"], # boat Yellow1		
    ["#C0A36E", "#717C7C"], # boat Yellow2		
    ["#E6C384", "#717C7C"], # carp Yellow		
    ["#D27E99", "#717C7C"], # sakura Pink		
    ["#E46876", "#717C7C"], # wave Red          
    ["#FF5D62", "#717C7C"], # peach Red         
    ["#FFA066", "#717C7C"], # surimi Orange		
    ["#717C7C", "#717C7C"], # katana Gray		
    ["#54536D", "#4e4e4e"] # comet            )
    # Source - https://github.com/rebelot/kanagawa.nvim/blob/master/extras/kanagawa-theme.el
    ]

GruvboxDark  = [
    ["#282828", "#282828"], # bg
    ["#ebdbb2", "#ebdbb2"], # fg
    ["#000000", "#000000"], # color01
    ["#fb4934", "#fb4934"], # color02
    ["#98971a", "#98971a"], # color03
    ["#d79921", "#d79921"], # color04
    ["#83a598", "#83a598"], # color05
    ["#d3869b", "#d3869b"], # color06
    ["#b8bb26", "#b8bb26"], # color11
    ]
MonokaiPro = [
    ["#2D2A2E", "#2D2A2E"], # bg
    ["#FCFCFA", "#FCFCFA"], # fg
    ["#403E41", "#403E41"], # color01
    ["#FF6188", "#FF6188"], # color02
    ["#A9DC76", "#A9DC76"], # color03
    ["#FFD866", "#FFD866"], # color04
    ["#FC9867", "#FC9867"], # color05
    ["#AB9DF2", "#AB9DF2"], # color06
    ["#78DCE8", "#78DCE8"]  # color07
    ]

Nord = [
    ["#2E3440", "#2E3440"], # bg
    ["#D8DEE9", "#D8DEE9"], # fg
    ["#3B4252", "#3B4252"], # color01
    ["#BF616A", "#BF616A"], # color02
    ["#A3BE8C", "#A3BE8C"], # color03
    ["#EBCB8B", "#EBCB8B"], # color04
    ["#81A1C1", "#81A1C1"], # color05
    ["#B48EAD", "#B48EAD"], # color06
    ["#88C0D0", "#88C0D0"]  # color07
    ]

OceanicNext = [
    ["#1b2b34", "#1b2b34"], # bg
    ["#d8dee9", "#d8dee9"], # fg
    ["#29414f", "#29414f"], # color01
    ["#ec5f67", "#ec5f67"], # color02
    ["#99c794", "#99c794"], # color03
    ["#fac863", "#fac863"], # color04
    ["#6699cc", "#6699cc"], # color05
    ["#c594c5", "#c594c5"], # color06
    ["#5fb3b3", "#5fb3b3"]  # color07
    ]

Palenight = [
    ["#292d3e", "#292d3e"], # bg
    ["#d0d0d0", "#d0d0d0"], # fg
    ["#434758", "#434758"], # color01
    ["#f07178", "#f07178"], # color02
    ["#c3e88d", "#c3e88d"], # color03
    ["#ffcb6b", "#ffcb6b"], # color04
    ["#82aaff", "#82aaff"], # color05
    ["#c792ea", "#c792ea"], # color06
    ["#89ddff", "#89ddff"]  # color15
    ]

SolarizedDark = [
    ["#002b36", "#002b36"], # bg
    ["#839496", "#839496"], # fg
    ["#073642", "#073642"], # color01
    ["#dc322f", "#dc322f"], # color02
    ["#859900", "#859900"], # color03
    ["#b58900", "#b58900"], # color04
    ["#268bd2", "#268bd2"], # color05
    ["#d33682", "#d33682"], # color06
    ["#2aa198", "#2aa198"]  # color15
    ]

SolarizedLight = [
    ["#fdf6e3", "#fdf6e3"], # bg
    ["#657b83", "#657b83"], # fg
    ["#ece5ac", "#ece5ac"], # color01
    ["#dc322f", "#dc322f"], # color02
    ["#859900", "#859900"], # color03
    ["#b58900", "#b58900"], # color04
    ["#268bd2", "#268bd2"], # color05
    ["#d33682", "#d33682"], # color06
    ["#2aa198", "#2aa198"]  # color15
    ]

TomorrowNight = [
    ["#1d1f21", "#1d1f21"], # bg
    ["#c5c8c6", "#c5c8c6"], # fg
    ["#373b41", "#373b41"], # color01
    ["#cc6666", "#cc6666"], # color02
    ["#b5bd68", "#b5bd68"], # color03
    ["#e6c547", "#e6c547"], # color04
    ["#81a2be", "#81a2be"], # color05
    ["#b294bb", "#b294bb"], # color06
    ["#70c0ba", "#70c0ba"]  # color15
    ]
